# README #

This is a public repo intended to run on an unauthenticated EC2 instance as part of it's userdata startup script.

It expects 2 environment variables to be set:

<table>
<thead>
  <tr>
    <th>Environment variable</th>
    <th>Usage</th>
    <th>Example</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>ENVIRONMENT_REPO</td>
    <td>An environment git repository to pull using SSH</td>
    <td>:REPO_OWNER/REPO_NAME.git</td>
  </tr>
  <tr>
    <td>NOTIFICATION_WEBHOOK_URL</td>
    <td>A URL for Microsoft Teams' Webhook connector. The script will send notifications to this URL including the public SSH key to register.</td>
    <td>https://synthesissoftware.webhook.office.com/webhookb2/GUID1/IncomingWebhook/GUID2/GUID3</td>
  </tr>
</tbody>
</table>


## Usage ##
In your CloudFormation template, include *UserData* for the EC2 Instance:

```json
"UserData": {
  "Fn::Base64": "!/bin/bash\n\n export ENVIRONMENT_REPO='git@bitbucket.org:synthesis_admin/xstream-environment-[ID].git'\n\n "
},
```
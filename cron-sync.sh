#!/usr/bin/bash

WEBHOOK_NOTIFY_URL=$(cat .webhook_url)

NOTIFY() {
    echo -e "$1"
    if [ -n "$WEBHOOK_NOTIFY_URL" ]; then
        curl -H 'Content-Type: application/json' -d "{ \"text\": \"$1\" }" $WEBHOOK_NOTIFY_URL
    fi
}

ADD_TEXT_IF_NOT_EXISTS() {
    LINE="$1"
    FILE="$2"
    if test -f $FILE; then
        grep -qF "$LINE" "$FILE"  || echo "$LINE" | tee --append "$FILE"
    else
        NOTIFY "Cannot append to $FILE: file not found"
    fi
}

GIT_PULL() {
    NOTIFY "Automatic pull in $PWD"
    local CURRENT_HEAD=$(git rev-parse --short HEAD)
    # Fix permissions
    chown $USER:$USER . -R || sudo chown $USER:$USER . -R
    
    echo "Pulling repo changes..."
    git pull --rebase origin
    local NEW_HEAD=$(git rev-parse --short HEAD)
    
    if [ "$CURRENT_HEAD" = "$NEW_HEAD" ]; then
        return 1 #false
    else
        echo return 0 # true
    fi
}

UNCOMMITTED_CHANGES() {
    if [[ -z $(git status -s) ]]; then
        return 0
    else
        return 1
    fi
}

GIT_PUSH() {
    NOTIFY "Automatic push in $PWD"

    git config user.name "synthesisbuilduser"
    git config user.email "synthesisbuilduser@synthesisbuilduser.co.za"
    git config push.default simple
    
    CLIENT_NAME=$(cat environment-manifest.json | jq -r ".CLIENT_NAME")
    
    # Create the timestamp for this commit:
    export GIT_TIMESTAMP=$(date +"%Y-%m-%d-%H-%M-%S")
    
    # Get the name of the current branch from Git, since environment variables can't be shared across processes while they are running:
    GIT_BRANCH=$(git symbolic-ref --short HEAD)
    
    # Fix permissions
    chown $USER:$USER . -R || sudo chown $USER:$USER . -R
    
    # Add all the changes:
    # NOTE: if there were no changes then this silently fails.
    git add .
    
    # Commit the changes:
    # NOTE: if there were no changes then this silently fails.
    git commit --message="Cron-sync at $GIT_TIMESTAMP"
    
    # Push the changes to the origin:
    git push --set-upstream origin "$GIT_BRANCH"
}

STATEFILE="local.statefile"
ADD_TEXT_IF_NOT_EXISTS "$STATEFILE" ".gitignore"

# Pull any remote updates
if GIT_PULL; then
    echo "PULLED" > "$STATEFILE"
fi

# Push any local updates
if UNCOMMITTED_CHANGES; then
    echo "PUSHED" > "$STATEFILE"
    GIT_PUSH
fi
#!/bin/bash

# shellcheck disable=SC2004
if (($EUID == 0)); then
  echo -e "FATAL: Cannot be run as root, run as standard user in sudo group"
  exit 1
fi

NOTIFY() {
  local NOW_STR="$(date -d '+2 hours' '+%Y-%m-%d %H-%M-%S')"

  if [ -z "$1" ]; then
    echo -e "Invalid call to NOTIFY, message expected."
    exit 1
  fi

  local RAW_TEXT="$1"
  local UTF8_TEXT=$(echo "$RAW_TEXT" | iconv -t utf-8)

  echo -e "$NOW_STR: $UTF8_TEXT" >>notify.glog
  echo -e "$NOW_STR: $UTF8_TEXT"
  . /etc/os-release
  ROR_HOSTNAME=$(hostname)
  ROR_USER=$(whoami)
  ROR_DISTRO=$NAME
  ROR_CURRENT_DIR=$PWD

  ROR_NOTIFY_MESSAGE="{ \"ror_message\": \"$UTF8_TEXT\", \"hostname\": \"$ROR_HOSTNAME\", \"user\": \"$ROR_USER\", \"distro\": \"$ROR_DISTRO\", \"current_dir\": \"$ROR_CURRENT_DIR\" }"
  curl --location \
    --request POST "https://sdpavhriki.execute-api.eu-west-1.amazonaws.com/default/py-ror-notifications" \
    --header 'Content-Type: application/json' \
    --data-raw "$ROR_NOTIFY_MESSAGE"
}

RUN_SCRIPTS_IN_ORDER() {
  SCRIPT_DIR="$1"
  cd $SCRIPT_DIR
  ORDERED_SCRIPTS=$(ls -1 | grep .sh)
  for script in $ORDERED_SCRIPTS; do

    if [[ $script =~ ^[[:digit:]] && $script == *.sh ]]; then
      SCRIPT_LOG="$script.log"
      chmod +x $script
      if [[ "$script" == *".root."* ]]; then
        NOTIFY "Starting script as root: $script"
        sudo ./$script | tee -a $SCRIPT_LOG
      elif [[ "$script" == *".nonroot."* ]]; then
        NOTIFY "Starting script as nonroot ($(whoami)): $script"
        ./$script | tee -a $SCRIPT_LOG
      else
        NOTIFY "Ignoring $script as it does not follow the naming convention"
      fi

      SCRIPT_OUTPUT=$(cat $SCRIPT_LOG)
      NOTIFY "$SCRIPT_OUTPUT"
    fi
  done
  cd -
}

RUN_CMD_AS_USER() {
  RUN_USER="$1"
  RUN_CMD="$2"
  sudo -H -u "$RUN_USER" bash -c "$RUN_CMD"
}

GIT_URL_TO_REPO_NAME() {
  local REPO_NAME=$(basename "$1" .git)
  echo "$REPO_NAME"
}

CLONE_REPO_RETRY() {
  DONE_STRING="already exists and is not an empty directory."
  ACCESS_DENIED_STRING="Permission denied (publickey)"

  local REPO_URL="$1"
  local BASE_PATH="$2"
  local REPO_DIR=$(GIT_URL_TO_REPO_NAME "$REPO_URL")
  local REPO_PATH="$BASE_PATH/$REPO_DIR"
  local CLONE_LOG="$BASE_PATH/$REPO_DIR-clone.log"
  local CLONED_SUCCESS=0
  local BLOCK_NOTIFY_FLAG=0

  NOTIFY "Cloning $REPO_DIR into $BASE_PATH/$REPO_DIR, will retry until SSH Public key is registered"
  while [ $CLONED_SUCCESS -eq 0 ]; do
    mkdir -p $BASE_PATH
    # Attempt to clone repo as standard user and read result
    CLONE_CMD="GIT_SSH_COMMAND='ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no' && cd $BASE_PATH && git clone --depth 1 $REPO_URL $REPO_DIR &>> $CLONE_LOG"

    RUN_CMD_AS_USER "$RUNTIME_USER" "$CLONE_CMD"
    CLONE_OUTPUT=$(cat "$CLONE_LOG")

    # If git output contains 'done string', it cloned successfully
    if [[ "$CLONE_OUTPUT" == *"$DONE_STRING"* ]]; then
      CLONED_SUCCESS=1
      NOTIFY "Cloned $REPO_DIR successfully into $REPO_PATH"
    # If git output contains 'denied string', it did not clone successfully
    fi

    if [[ "$CLONE_OUTPUT" == *"$ACCESS_DENIED_STRING"* ]]; then
      if [ $BLOCK_NOTIFY_FLAG -eq 0 ]; then
        NOTIFY "Clone access denied"
        BLOCK_NOTIFY_FLAG=1
      fi
    fi

    if [ $CLONED_SUCCESS -eq 0 ]; then
      sleep 15
    fi
  done
}

GET_DIR_FROM_PATH() {
  local FILE_NAME="$1"
  echo $(dirname $(readlink -f "$FILE_NAME"))
}

GET_FILENAME_FROM_PATH() {
  local FILE_NAME="$1"
  echo $(basename -- $FILE_NAME)
}

REGISTER_CRON() {
  CRON_TIMING="$1"
  CRON_SCRIPT_PATH="$2"

  if test -f "$CRON_SCRIPT_PATH"; then
    CRON_WORKING_DIR=$(GET_DIR_FROM_PATH "$CRON_SCRIPT_PATH")
    CRON_SCRIPT=$(GET_FILENAME_FROM_PATH "$CRON_SCRIPT_PATH")
    CRON_STRING="$CRON_TIMING cd $CRON_WORKING_DIR && chmod +x $CRON_SCRIPT && ./$CRON_SCRIPT"
    (
      sudo crontab -u "$RUNTIME_USER" -l
      echo "$CRON_STRING"
    ) | sort -u | sudo crontab -u "$RUNTIME_USER" -

  fi
}

FIND_CRON_SCRIPTS() {
  SCRIPT_DIR="$1"
  cd $SCRIPT_DIR
  ORDERED_SCRIPTS=$(ls -1 | grep .sh)
  for script in $ORDERED_SCRIPTS; do

    if [[ "$script" == *"cron-"* ]]; then
      CRON_PATH="$PWD/$script"

      if [[ "$script" == *"push"* ]]; then
        NOTIFY "Registering $CRON_PATH with */2 * * * *"
        REGISTER_CRON "*/2 * * * *" "$CRON_PATH"
      elif [[ "$script" == *"pull"* ]]; then
        NOTIFY "Registering $CRON_PATH with 1-59/2 * * * *"
        REGISTER_CRON "1-59/2 * * * *" "$CRON_PATH"
      else
        NOTIFY "Registering $CRON_PATH with */5 * * * *"
        REGISTER_CRON "*/5 * * * *" "$CRON_PATH"
      fi
    fi
  done
  ALL_CRONS=$(sudo crontab -u "$RUNTIME_USER" -l)
  NOTIFY "Registered Jobs: \n$ALL_CRONS"
  cd -
}

COMMIT_ROBOT_TIME() {
  RUN_ID=$1
  SEC=$2
  echo -e "\n$RUN_ID,$SEC" >>runtime.log
  git config user.name "synthesisbuilduser"
  git config user.email "synthesisbuilduser@synthesisbuilduser.co.za"
  git add runtime.log
  git commit -m "Runtime log: $RUN_ID"
  git push
}

RUNTIME_USER=$(whoami)
INIT_MESSAGE="Starting Init script as $RUNTIME_USER for ENVIRONMENT_REPO:\n\n$ENVIRONMENT_REPO"
NOTIFY "$INIT_MESSAGE"

INIT_JSON_PATH="/home/$RUNTIME_USER/rorinit.json"
echo -e "Parsing JSON expected at $INIT_JSON_PATH"

ENVIRONMENT_REPO=$(cat "$INIT_JSON_PATH" | jq -r ".ENVIRONMENT_REPO")
DEPLOYMENT_REPO=$(cat "$INIT_JSON_PATH" | jq -r ".DEPLOYMENT_REPO")
NOTIFICATION_WEBHOOK_URL=$(cat "$INIT_JSON_PATH" | jq -r ".NOTIFICATION_WEBHOOK_URL")

if [[ ! -v ENVIRONMENT_REPO ]]; then
  echo -e "FATAL: Key 'ENVIRONMENT_REPO' not set. Is it provided in '$INIT_JSON_PATH'?"
  exit 1
fi

if [[ ! -v DEPLOYMENT_REPO ]]; then
  echo -e "FATAL: Key 'DEPLOYMENT_REPO' not set. Is it provided in '$INIT_JSON_PATH'?"
  exit 1
fi

if [[ ! -v NOTIFICATION_WEBHOOK_URL ]]; then
  echo -e "FATAL: Key 'NOTIFICATION_WEBHOOK_URL' not set. Is it provided in '$INIT_JSON_PATH'?"
  exit 1
fi

# template replacement approach to escape quoting hell
# we also can't use native bash replacement syntax '$ { }' since cloudformation tries to substitute fields with the same syntax
SSH_KEYGEN_TEMPLATE='< /dev/zero ssh-keygen -f /home/--USER--/.ssh/id_rsa -q -N ""'
# shellcheck disable=SC2001
SSH_KEYGEN_CMD=$(echo "$SSH_KEYGEN_TEMPLATE" | sed "s/--USER--/$RUNTIME_USER/")
RUN_CMD_AS_USER "$RUNTIME_USER" "$SSH_KEYGEN_CMD"
RUN_CMD_AS_USER "$RUNTIME_USER" "ssh-keyscan bitbucket.org >> /home/$RUNTIME_USER/.ssh/known_hosts"
GENERATED_PUB_KEY=$(cat "/home/$RUNTIME_USER/.ssh/id_rsa.pub")
NOTIFY "To authorize machine $(cat /etc/hostname), register following PUBLIC key to BitBucket: https://bitbucket.org/account/settings/ssh-keys/ \n\n$GENERATED_PUB_KEY"

USER_HOME=$(getent passwd "$RUNTIME_USER" | cut -d: -f6)
STARTING_DIR="$PWD"

NOTIFY "Starting directory set to: $STARTING_DIR"

NOTIFY "Cloning repositories"
CLONE_REPO_RETRY "$ENVIRONMENT_REPO" "$STARTING_DIR"
CLONE_REPO_RETRY "$DEPLOYMENT_REPO" "$STARTING_DIR"

ENV_REPO_DIR="$STARTING_DIR/$(GIT_URL_TO_REPO_NAME $ENVIRONMENT_REPO)"
DEP_REPO_DIR="$STARTING_DIR/$(GIT_URL_TO_REPO_NAME $DEPLOYMENT_REPO)"

NOTIFY "Starting scripts"
RUN_SCRIPTS_IN_ORDER "$ENV_REPO_DIR"
RUN_SCRIPTS_IN_ORDER "$DEP_REPO_DIR"

NOTIFY "Adding cron scripts"
/usr/bin/cp cron-*.sh "$ENV_REPO_DIR"
/usr/bin/cp cron-*.sh "$DEP_REPO_DIR"